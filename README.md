# Java Drink Trade AWT Desktop Application #
- Full half-year University project (2013),
- FrontEnd: Java AWT + JDBC connector to Oracle RDBMS,
- BackEnd: Oracle RDBMS fully configured
### /Ablak ###
* Contains Main Java AWT Frontend Desktop Application
### /ORACLE_Backend ###
* Contains Oracle Table Creations, 
* Test Data Uploads, 
* PL/SQL package implementations (+ data logging!!)
* Database figures
### ojdbc6.jar ###
* Oracle JDBC Driver Connector
### +Frontend Screenshots ###