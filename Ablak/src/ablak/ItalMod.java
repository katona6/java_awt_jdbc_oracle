package ablak;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.event.*;

public class ItalMod extends JDialog implements ActionListener
{
    private JPanel dp;
    private JButton bo,bs;
    private Szallitomj dtmp1;
    private int kilep=0;
    private JTextField ek1, ek2, ek3, ek4, ek5, ek6, ek7;
    static String up="Üzenetpanel";
    static int jel=0;
    
    public ItalMod(JFrame f, Szallitomj dtmp, int bsx, int bsy)
    {
        super(f,"O_Ital tábla - Módosítás",true);
        
        dtmp1=dtmp;
        setBounds(bsx-30,bsy+30,900,500);
        dp = new JPanel();
        dp.setLayout(null);
        this.getContentPane().add(dp);
        
        JLabel l1 = new JLabel("Jelölje ki a módosítandó Italt, és töltse ki a módosítandó mezőket!");
        dp.add(l1);
        l1.setBounds(10,5,450,20);
        
        JTable table = new JTable( dtmp1 );
        TableColumn column = null;
        for (int i = 0; i < 9; i++)
        {
            column = table.getColumnModel().getColumn(i);
            if (i == 0) {column.setPreferredWidth(10);}        //Jelolo
            else if (i == 1) {column.setPreferredWidth(100);}   //Vonalkod
            else if (i == 2) {column.setPreferredWidth(120);}   //Nev
            else if (i == 3) {column.setPreferredWidth(50);}    //Gyarto
            else if (i == 4) {column.setPreferredWidth(35);}    //Ar
            else if (i == 5) {column.setPreferredWidth(20);}    //Alkohol
            else if (i == 6) {column.setPreferredWidth(30);}    //Kiszereles
            else if (i == 7) {column.setPreferredWidth(70);}    //Szarmazasi_hely
            else {column.setPreferredWidth(70);}               //Szallito
        }
        JScrollPane sp;
        sp = new JScrollPane(table);
        dp.add(sp);
        sp.setBounds(10,30,860,300);
        
        JLabel l2 = new JLabel("Új adatok:");
        dp.add(l2);
        l2.setBounds(10,340,70,20);
        
        ek1 = new JTextField("",10);
        dp.add(ek1);
        ek1.setBounds(200,340,158,20);
        
        ek2 = new JTextField("",10);
        dp.add(ek2);
        ek2.setBounds(358,340,88,20);

        ek3 = new JTextField("",10);
        dp.add(ek3);
        ek3.setBounds(446,340,70,20);
        
        ek4 = new JTextField("",10);
        dp.add(ek4);
        ek4.setBounds(516,340,58,20);
        
        ek5 = new JTextField("",10);
        dp.add(ek5);
        ek5.setBounds(574,340,67,20);
        
        ek6 = new JTextField("",10);
        dp.add(ek6);
        ek6.setBounds(641,340,108,20);
        
        ek7 = new JTextField("",10);
        dp.add(ek7);
        ek7.setBounds(749,340,106,20);
         
        
        bo = new JButton();
        bo.setText("Módosít");
        dp.add(bo);
        bo.setBounds(140,360,80,20);
        bo.addActionListener( this );
        
        bs = new JButton();
        bs.setText("Mégse");
        dp.add(bs);
        bs.setBounds(230,360,80,20);
        bs.addActionListener( this );
        
    }
    
    public void actionPerformed( ActionEvent e )
    {
        if (e.getSource() == this.bo)
        {
            int db=0, x=0;
            for(x = 0; x < dtmp1.getRowCount(); x++)
            if ((Boolean)dtmp1.getValueAt(x,0)) {db++; jel=x;}
            if (db==0) JOptionPane.showMessageDialog(null, "Egy adatsort ki kell jelölni!",up, 2);
            if (db>1) JOptionPane.showMessageDialog(null, "Csak egy adatsort jelöljön ki!",up, 2);
            if (db==1)
            {
                kilep=2; this.dispose(); setVisible(false);
            }

        }
        if (e.getSource() == this.bs) {kilep=3; this.dispose(); setVisible(false);}
    }
    
    public int kiLep(){return kilep;}
    public int getJel(){return jel;}
    public String getNev(){return ek1.getText();}
    public String getGyarto(){return ek2.getText();}
    public String getAr(){return ek3.getText();}
    public String getAlkohol(){return ek4.getText();}
    public String getKiszereles(){return ek5.getText();}
    public String getSzarmhely(){return ek6.getText();}
    public String getSzallito(){return ek7.getText();}
    

    
    
}
