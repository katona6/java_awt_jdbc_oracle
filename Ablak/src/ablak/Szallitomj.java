
package ablak;
import javax.swing.table.DefaultTableModel;

public class Szallitomj extends DefaultTableModel
{
    public Szallitomj (Object Mezonevek[], int rows)
    {
        super(Mezonevek, rows);
    }
    
    @Override
    public boolean isCellEditable(int row, int col)
    { //Módosíthatóság megadása
        if (col == 0) {return true;}
        return false;
    }
    
    @Override
    public Class<?> getColumnClass(int index)
    { //Oszlopok típusának megadása
        if(index==0){return(Boolean.class);} //Logikai 0/1
        if(index > 4){return(Integer.class);} //Egész
        return(String.class); //Szöveg    
    }
}
