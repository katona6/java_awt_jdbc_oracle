package ablak;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.event.*;

public class VevoTorol extends JDialog implements ActionListener
{
    private JPanel dp;
    private JButton bo,bs;
    private Szallitomj dtmp1;
    private int kilep=0;

    public VevoTorol(JFrame f, Szallitomj dtmp, int bsx, int bsy)
    {
        super(f,"O_Vevo tábla - Törlés",true);
        dtmp1=dtmp;
        setBounds(bsx-30,bsy+30,900,500);
        dp = new JPanel();
        dp.setLayout(null);
        this.getContentPane().add(dp);
        
        JLabel l1 = new JLabel("Jelölje ki a törlendő Vevöket!");
        dp.add(l1);
        l1.setBounds(10,5,260,20);
        
        JTable table = new JTable( dtmp1 );
        TableColumn column = null;
        for (int i = 0; i < 8; i++)
        {
            column = table.getColumnModel().getColumn(i);
            if (i == 0) {column.setPreferredWidth(10);}        //Jelolo
            else if (i == 1) {column.setPreferredWidth(80);}   //Szemigszam
            else if (i == 2) {column.setPreferredWidth(120);}    //Nev
            else if (i == 3) {column.setPreferredWidth(35);}    //irszam
            else if (i == 4) {column.setPreferredWidth(70);}    //Varos
            else if (i == 5) {column.setPreferredWidth(80);}    //Utca
            else if (i == 6) {column.setPreferredWidth(30);}    //Hsz
            else {column.setPreferredWidth(70);}               //Foglalkozas
        }
        
        JScrollPane sp;
        sp = new JScrollPane(table);
        dp.add(sp);
        sp.setBounds(10,30,860,300);
        
        bo = new JButton();
        bo.setText("Töröl");
        dp.add(bo);
        bo.setBounds(140,360,80,20);
        bo.addActionListener( this );
        
        bs = new JButton();
        bs.setText("Mégse");
        dp.add(bs);
        bs.setBounds(230,360,80,20);
        bs.addActionListener( this );
    }
    
    public void actionPerformed( ActionEvent e )
    {
        if (e.getSource() == this.bo) {kilep=2; this.dispose(); setVisible(false);}
        if (e.getSource() == this.bs) {kilep=3; this.dispose(); setVisible(false);}
    }
    
    public int kiLep(){return kilep;}
    public boolean getJel(int x){return (Boolean)dtmp1.getValueAt(x,0);}
    
    
}
