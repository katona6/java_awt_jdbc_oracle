package ablak;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

public class VevoBe extends JDialog implements ActionListener
{
    private JTextField ek1, ek2, ek3, ek4, ek5, ek6, ek7;
    private JSpinner jsp;
    
    private JPanel dp;
    private JButton bo, bs;
    private String up="Üzenetpanel";
    private int kilep=0;

    public VevoBe(JDialog f, int bsx, int bsy)
    {
        super(f,"Vevö beszúrás",true);
        setBounds(bsx+25,bsy+30,350,370);
        dp = new JPanel();
        dp.setLayout(null);
        this.getContentPane().add( dp);
        
        JLabel l1 = new JLabel("Szemigszam:");
        dp.add(l1);
        l1.setBounds(20,20,80,20);
        ek1 = new JTextField("",10);
        dp.add(ek1);
        ek1.setBounds(140,20,80,20);

        
        JLabel l2 = new JLabel("Név:");
        dp.add(l2);
        l2.setBounds(20,50,100,20);
        ek2 = new JTextField("",10);
        dp.add(ek2);
        ek2.setBounds(120,50,80,20);

        JLabel l3 = new JLabel("Ir_Szám:");
        dp.add(l3);
        l3.setBounds(20,80,100,20);
        ek3 = new JTextField("",10);
        dp.add(ek3);
        ek3.setBounds(120,80,200,20);

        
        JLabel l4 = new JLabel("Város:");
        dp.add(l4);
        l4.setBounds(20,110,100,20);
        ek4 = new JTextField("",10);
        dp.add(ek4);
        ek4.setBounds(120,110,100,20);
        
        JLabel l5 = new JLabel("Utca:");
        dp.add(l5);
        l5.setBounds(20,140,100,20);
        ek5 = new JTextField("",10);
        dp.add(ek5);
        ek5.setBounds(120,140,100,20);
        ek5.setHorizontalAlignment(JTextField.RIGHT);
        
        JLabel l6 = new JLabel("Hsz:");
        dp.add(l6);
        l6.setBounds(20,170,100,20);
        ek6 = new JTextField("",10);
        dp.add(ek6);
        ek6.setBounds(120,170,80,20);
        ek6.setHorizontalAlignment(JTextField.CENTER);
        
        JLabel l7 = new JLabel("Foglalkozás:");
        dp.add(l7);
        l7.setBounds(20, 200, 80, 20);
        ek7 = new JTextField("",10);
        dp.add(ek7);
        ek7.setBounds(120,200,80,20);
        ek7.setHorizontalAlignment(JTextField.RIGHT);
        
        bo = new JButton();
        bo.setText(" OK");
        dp.add(bo);
        bo.setBounds(145,280,60,30);
        bo.addActionListener( this );
        
        bs = new JButton();
        bs.setText("Mégse");
        dp.add(bs);
        bs.setBounds(210,280,80,30);
        bs.addActionListener( this );        
        
    }
    
    public void actionPerformed( ActionEvent e ) 
    {
        if (ek1.getText().length() == 0)
        {JOptionPane.showMessageDialog(null, "Hiányzó Szemigszam!",up,2);}
        else if (ek2.getText().length() == 0)
        {JOptionPane.showMessageDialog(null, "Hiányzó NÉV!",up,2);}
        else if (ek3.getText().length() == 0)
        {JOptionPane.showMessageDialog(null, "Hiányzó IR_SZÁM!",up,2);}
        else if (ek4.getText().length() == 0)
        {JOptionPane.showMessageDialog(null, "Hiányzó VÁROS!",up,2);}
        else if (ek5.getText().length() == 0)
        {JOptionPane.showMessageDialog(null, "Hiányzó UTCA!",up,2);}
        else if (ek6.getText().length() == 0)
        {JOptionPane.showMessageDialog(null, "Hiányzó HSZ!",up,2);} 
        else if (ek7.getText().length() == 0)
        {JOptionPane.showMessageDialog(null, "Hiányzó FOGLALKOZAS!",up,2);}  
           
        else if (!joSzam(ek3.getText()))
        {JOptionPane.showMessageDialog(null, "Hibásan beírt IR_SZÁM!",up,2);}
        else if (!joSzam(ek6.getText()))
        {JOptionPane.showMessageDialog(null, "Hibásan beírt HSZ!",up,2);}
        else { kilep=2; this.dispose(); setVisible(false); }
        if (e.getSource() == this.bs) {kilep=3; this.dispose(); setVisible(false);}
    }
    
    public boolean joSzam(String i)
    { //----------------------------Jószám?
        try
        {
            Integer.parseInt(i);
            return true;
        }
        catch(NumberFormatException nfe)
        {
            return false;
        }
    }
    
public String getSzemigszam(){return ek1.getText();}
public String getNev(){return ek2.getText();}
public int getIr_Szam(){return Integer.parseInt(ek3.getText());}
public String getVaros(){return ek4.getText();}
public String getUtca(){return ek5.getText();}
public String getHsz(){return ek6.getText();}
public String getFoglalkozas(){return ek7.getText();}
public int kiLep(){return kilep;}    
    
    
    
}
