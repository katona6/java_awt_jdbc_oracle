
package ablak;
import javax.swing.table.DefaultTableModel;

public class Szallitom extends DefaultTableModel
{
    public Szallitom (Object Mezonevek[], int rows)
    {
        super(Mezonevek, rows);
    }
    
    @Override
    public boolean isCellEditable(int row, int col)
    {
        return false;
    }
    
    @Override
    public Class<?> getColumnClass(int index)
    {
        if(index > 3){return(Integer.class);}
        return(String.class);
    }
}
