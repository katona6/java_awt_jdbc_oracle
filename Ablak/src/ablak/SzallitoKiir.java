
package ablak;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.event.*;

public class SzallitoKiir extends JDialog implements ActionListener
{
    private JPanel dp;
    private JButton bo,bs;
    private Szallitom dtmp1;
    private int kilep=0;
    
    public SzallitoKiir(JFrame f, Szallitom dtmp, int bsx, int bsy)
    {
        super(f,"O_Szallito tábla - Listázás",true);
        dtmp1=dtmp;
        setBounds(bsx-300,bsy-100,900,500);
        dp = new JPanel();
        dp.setLayout(null);
        this.getContentPane().add( dp);
        JTable table = new JTable( dtmp1 );
        TableColumn column = null;
        for (int i = 0; i < 9; i++)
        {
            //Mezöhosszúságok Beállitása
            column = table.getColumnModel().getColumn(i);
            if (i == 0) {column.setPreferredWidth(120);}        //Cegjegyzekszam
            else if (i == 1) {column.setPreferredWidth(120);}   //Nev
            else if (i == 2) {column.setPreferredWidth(50);}    //Ir_szam
            else if (i == 3) {column.setPreferredWidth(70);}    //Varos
            else if (i == 4) {column.setPreferredWidth(80);}    //Utca
            else if (i == 5) {column.setPreferredWidth(30);}    //HSZ
            else if (i == 6) {column.setPreferredWidth(30);}    //Tel
            else if (i == 7) {column.setPreferredWidth(70);}    //Web
            else {column.setPreferredWidth(70);}               //Email
        }
        
        table.setAutoCreateRowSorter(true);
        TableRowSorter<Szallitom> sorter = (TableRowSorter<Szallitom>)table.getRowSorter();
        JScrollPane sp;
        sp = new JScrollPane(table);
        dp.add(sp);
        sp.setBounds(10,10,860,300);
        bo = new JButton();
        bo.setText(" Bezár");
        dp.add(bo);
        bo.setBounds(400,350,90,20);
        bo.addActionListener( this );
    }
    
    public void actionPerformed( ActionEvent e )
    {
        if (e.getSource() == this.bo) {kilep=2; this.dispose(); setVisible(false);}
    }
    public int kiLep(){return kilep;}
}
    
        

