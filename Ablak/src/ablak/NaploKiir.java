
package ablak;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.event.*;

public class NaploKiir extends JDialog implements ActionListener
{
    private JPanel dp;
    private JButton bo,bs;
    private Szallitom dtmp1;
    private int kilep=0;
    
    public NaploKiir(JFrame f, Szallitom dtmp, int bsx, int bsy)
    {
        super(f,"O_Naplo",true);
        dtmp1=dtmp;
        setBounds(bsx-300,bsy-100,900,500);
        dp = new JPanel();
        dp.setLayout(null);
        this.getContentPane().add( dp);
        JTable table = new JTable( dtmp1 );
        TableColumn column = null;
        for (int i = 0; i < 5; i++)
        {
            //Mezöhosszúságok Beállitása
            column = table.getColumnModel().getColumn(i);
            if (i == 0) {column.setPreferredWidth(120);}        //Muvelet
            else if (i == 1) {column.setPreferredWidth(120);}   //Usernev
            else if (i == 2) {column.setPreferredWidth(50);}    //Idopont
            else if (i == 3) {column.setPreferredWidth(70);}    //Esemeny
            else {column.setPreferredWidth(70);}               //Status
        }
        
        table.setAutoCreateRowSorter(true);
        TableRowSorter<Szallitom> sorter = (TableRowSorter<Szallitom>)table.getRowSorter();
        JScrollPane sp;
        sp = new JScrollPane(table);
        dp.add(sp);
        sp.setBounds(10,10,860,300);
        bo = new JButton();
        bo.setText(" Bezár");
        dp.add(bo);
        bo.setBounds(400,350,90,20);
        bo.addActionListener( this );
    }
    
    public void actionPerformed( ActionEvent e )
    {
        if (e.getSource() == this.bo) {kilep=2; this.dispose(); setVisible(false);}
    }
    public int kiLep(){return kilep;}
}
    
        

