/*
 
 
    JTextField jtf1 = new JTextField("");
    dp.add(jtf1);
    jtf1.setBounds(70,20,80,20);
 
 */
package src.ablak;
import java.awt.*;
import java.awt.event.*;
import java.sql.*;
import javax.swing.*;


public class Ablak extends JFrame
{
    private JTabbedPane jtp;
    private JPanel dp, jp1, jp2, jp3, jp4;
    private JButton blist, bbeszur, bmodosit, btorol, commandbutton;
    private JTextField parancssor;
    JLabel pct1, pct2, pct3;
    static String up="Üzenetpanel"; //Üzenet panel felirata
    
    private static Szallitom dtmp; //Tábla modell kiíráshoz
    private static Szallitomj ttmp; //Tábla modell kijelöléshez
    
    static Connection conn = null;
    static String user="katona6";
    static String pswd="jelszo";
    static Statement s = null;
    static PreparedStatement ps = null;
    static ResultSet rs = null;
    

    public static void Kapcsolodas(String user, String pswd)
    {
        try 
        {
            String url = "jdbc:oracle:thin:@193.6.5.42:1521:info";
            conn = DriverManager.getConnection(url, user, pswd);
        } 
        catch(Exception ex)
        {
            JOptionPane.showMessageDialog(null,"Kapcsolódási hiba: "+ex.getMessage(),up,2);
        }
    }
    
    public static void Lekapcsolodas()
    {
        try
        {
            conn.close();
        }
        catch(Exception ex)
        {
            JOptionPane.showMessageDialog(null,"Kapcsolat lezárási hiba: "+ex.getMessage(),up,2);
        }
    }
    
    public static void SzallitoLista()
    { //-------------------Szállítók adatainak beolvasása
        String cj="", nev="", varos="", utca="", hsz="", tel="", web="", email=""; int irsz=0;
        String sqlp= "select * from O_Szallito";
        try
        {
            s = conn.createStatement();
            s.executeQuery(sqlp);
            rs = s.getResultSet();
            while(rs.next()) {    
            cj = rs.getString("Cegjegyzekszam").trim();
            nev = rs.getString("Nev").trim();
            irsz = rs.getInt("Ir_Szam");
            varos = rs.getString("Varos").trim();
            utca = rs.getString("Utca").trim();
            hsz = rs.getString("Hsz").trim();
            tel = rs.getString("Tel").trim();
            web = rs.getString("Web").trim();
            email = rs.getString("Email").trim();
            dtmp.addRow(new Object[ ]{ cj, nev, irsz, varos, utca, hsz, tel, web, email}); //Sor beszúrása a táblázatba
            }
            rs.close();
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, "Nem sikerült betölteni az adatokat!"+e.getMessage(), up, 2);
        }
    }
	
    public static void ItalLista()
    { //-------------------Italok adatainak beolvasása
        String vk="", nev="", gyarto="", szarmhely="", szallito=""; int ar=0, alkohol=0, kiszereles=0;
        String sqlp= "select * from O_Ital";
        try
        {
            s = conn.createStatement();
            s.executeQuery(sqlp);
            rs = s.getResultSet();
            while(rs.next()) {    
            vk = rs.getString("VONALKOD").trim();
            nev = rs.getString("NEV").trim();
            gyarto = rs.getString("GYARTO").trim();
            ar = rs.getInt("AR");
            alkohol = rs.getInt("ALKOHOL");
            kiszereles = rs.getInt("KISZERELES");
            szarmhely = rs.getString("SZARMAZASI_HELY").trim();
            szallito = rs. getString("SZALLITO").trim();
            
            dtmp.addRow(new Object[ ]{ vk, nev, gyarto, ar, alkohol, kiszereles, szarmhely, szallito}); //Sor beszúrása a táblázatba
            }
            rs.close();
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, "Nem sikerült betölteni az adatokat!"+e.getMessage(), up, 2);
        }
    }
	
    public static void VevoLista()
    { //-------------------Vevok adatainak beolvasása
        String szemigszam="", nev="", varos="", utca="", hsz="", foglalkozas=""; int irsz=0;
        String sqlp= "select * from O_Vevo";
        try
        {
            s = conn.createStatement();
            s.executeQuery(sqlp);
            rs = s.getResultSet();
            while(rs.next()) {    
            szemigszam = rs.getString("SZEMIGSZAM").trim();
            nev = rs.getString("Nev").trim();
            irsz = rs.getInt("Ir_Szam");
            varos = rs.getString("Varos").trim();
            utca = rs.getString("Utca").trim();
            hsz = rs.getString("Hsz").trim();
            foglalkozas = rs.getString("FOGLALKOZAS").trim();

            dtmp.addRow(new Object[ ]{ szemigszam, nev, irsz, varos, utca, hsz, foglalkozas}); //Sor beszúrása a táblázatba
            }
            rs.close();
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, "Nem sikerült betölteni az adatokat!"+e.getMessage(), up, 2);
        }
    }	
    
    public static void SzallitoListaJel()
    {
        String cg="", nv="", varos="", utca="", hsz="", tel="", web="", email=""; int irsz=0;
        String sqlp= "select * from O_Szallito";
        try
        {
            s = conn.createStatement();
            s.executeQuery(sqlp);
            rs = s.getResultSet();
            while(rs.next())
            {
                cg = rs.getString(1).trim();
                nv = rs.getString(2).trim();
                irsz = rs.getInt(3);
                varos = rs.getString(4).trim();
                utca = rs.getString(5);
                hsz = rs.getString(6);
                tel = rs.getString(7);
                web = rs.getString(8);
                email = rs.getString(9);
                ttmp.addRow(new Object[]{ new Boolean(false), cg, nv, irsz, varos, utca, hsz, tel, web, email});
            }
            rs.close();
            
        }
        catch (Exception e) 
        {
            JOptionPane.showMessageDialog(null, "Nem sikerült betölteni az adatokat!"+e.getMessage(), up, 2);
        }
    }
	
    public static void ItalListaJel()
    {
        String vk="", nev="", gyarto="", szarmhely="", szallito=""; int ar=0, alkohol=0, kiszereles=0;
        String sqlp= "select * from O_Ital";
        try
        {
            s = conn.createStatement();
            s.executeQuery(sqlp);
            rs = s.getResultSet();
            while(rs.next())
            {
                vk = rs.getString("VONALKOD").trim();
                nev = rs.getString("NEV").trim();
                gyarto = rs.getString("GYARTO").trim();
                ar = rs.getInt("AR");
                alkohol = rs.getInt("ALKOHOL");
                kiszereles = rs.getInt("KISZERELES");
                szarmhely = rs.getString("SZARMAZASI_HELY").trim();
                szallito = rs. getString("SZALLITO").trim();
                ttmp.addRow(new Object[]{ new Boolean(false), vk, nev, gyarto, ar, alkohol, kiszereles, szarmhely, szallito});
            }
            rs.close();
            
        }
        catch (Exception e) 
        {
            JOptionPane.showMessageDialog(null, "Nem sikerült betölteni az adatokat!"+e.getMessage(), up, 2);
        }
    }
	
    public static void VevoListaJel()
    {
        String szemigszam="", nev="", varos="", utca="", hsz="", foglalkozas=""; int irsz=0;
        String sqlp= "select * from O_Vevo";
        try
        {
            s = conn.createStatement();
            s.executeQuery(sqlp);
            rs = s.getResultSet();
            while(rs.next())
            {
                szemigszam = rs.getString("SZEMIGSZAM").trim();
                nev = rs.getString("Nev").trim();
                irsz = rs.getInt("Ir_Szam");
                varos = rs.getString("Varos").trim();
                utca = rs.getString("Utca").trim();
                hsz = rs.getString("Hsz").trim();
                foglalkozas = rs.getString("FOGLALKOZAS").trim();
                ttmp.addRow(new Object[]{ new Boolean(false), szemigszam, nev, irsz, varos, utca, hsz, foglalkozas});
            }
            rs.close();
            
        }
        catch (Exception e) 
        {
            JOptionPane.showMessageDialog(null, "Nem sikerült betölteni az adatokat!"+e.getMessage(), up, 2);
        }
    }
    
    public static void SzallitoUpdate(String adat)
    {
        String sqlp="update O_Szallito set "+adat;
        try 
        {
            s = conn.createStatement(); s.executeUpdate(sqlp);            
        } 
        catch (Exception e) 
        {
            JOptionPane.showMessageDialog(null, "Nem sikerült módisítani az adatokat!"+e.getMessage(), up, 2);
        }
    }
	
    public static void ItalUpdate(String adat)
    {
        String sqlp="update O_Ital set "+adat;
        try 
        {
            s = conn.createStatement(); s.executeUpdate(sqlp);            
        } 
        catch (Exception e) 
        {
            JOptionPane.showMessageDialog(null, "Nem sikerült módisítani az adatokat!"+e.getMessage(), up, 2);
        }
    }
	
    public static void VevoUpdate(String adat)
    {
        String sqlp="update O_Vevo set "+adat;
        try 
        {
            s = conn.createStatement(); s.executeUpdate(sqlp);            
        } 
        catch (Exception e) 
        {
            JOptionPane.showMessageDialog(null, "Nem sikerült módisítani az adatokat!"+e.getMessage(), up, 2);
        }
    }
    
    public static void SzallitoTorol(String cg)
    {
        String sqlp="delete from O_Szallito where Cegjegyzekszam = '"+cg+"'";
        try
        {
            s = conn.createStatement(); s.executeUpdate(sqlp);
        }
        catch (Exception e) 
        {
            JOptionPane.showMessageDialog(null, "Nem sikerült törölni az adatokat!"+e.getMessage(),up, 2);
        }
    }
	
    public static void ItalTorol(String cg)
    {
        String sqlp="delete from O_Ital where VONALKOD = '"+cg+"'";
        try
        {
            s = conn.createStatement(); s.executeUpdate(sqlp);
        }
        catch (Exception e) 
        {
            JOptionPane.showMessageDialog(null, "Nem sikerült törölni az adatokat!"+e.getMessage(),up, 2);
        }
    }
	
    public static void VevoTorol(String cg)
    {
        String sqlp="delete from O_Vevo where SZEMIGSZAM = '"+cg+"'";
        try
        {
            s = conn.createStatement(); s.executeUpdate(sqlp);
        }
        catch (Exception e) 
        {
            JOptionPane.showMessageDialog(null, "Nem sikerült törölni az adatokat!"+e.getMessage(),up, 2);
        }
    }
    
    public static void NaploLista()
    {
        String muvelet="", usernev="", idopont="", esemeny=""; int status=0;
        String sqlp= "select * from O_Naplo order by Idopont";
        try
        {
            s = conn.createStatement();
            s.executeQuery(sqlp);
            rs = s.getResultSet();
            while(rs.next())
            {
                muvelet = rs.getString("Muvelet").trim();
                usernev = rs.getString("Usernev").trim();
                idopont = String.valueOf(rs.getDate("Idopont")) ;
                esemeny = rs.getString("Esemeny").trim();
                status = rs.getInt("Status");
                dtmp.addRow(new Object[ ]{ muvelet, usernev, idopont, esemeny, status}); //Sor beszúrása a táblázatba   
            }
            rs.close();
        }
        catch (Exception ex) 
        {
            JOptionPane.showMessageDialog(null, "Nem sikerült betölteni az adatokat!"+ex.getMessage(), up, 2);
        }
    }
    
    public Ablak() 
    {
        setTitle("Ital Nyílvántartó Rendszer");
        setSize(400,380);
        setLocationRelativeTo(null);
        dp = new JPanel();
        dp.setLayout(null);
        this.getContentPane().add( dp);
        jp1 = new JPanel();
        jp2 = new JPanel();
        jp3 = new JPanel();
        jp4 = new JPanel();
        
        jtp = new JTabbedPane();
        jtp.addTab("O_Szallito", jp1);
        jtp.addTab("O_Ital", jp2);
        jtp.addTab("O_Vevo", jp3);
        jtp.addTab("O_Naplo", jp4);
        dp.add(jtp);
        jtp.setBounds(10,10,360,200);
        
        //SQL PARANCSVÉGREHAJTO
        JLabel parancssorfelirat = new JLabel("SQL parancsvégrehajtó:");
        dp.add(parancssorfelirat);
        parancssorfelirat.setBounds(20,230,140,20);
        parancssor = new JTextField("",10);
        dp.add(parancssor);
        parancssor.setBounds(20,250,250,20);
        commandbutton = new JButton();
        commandbutton.setText("Végrehajt");
        dp.add(commandbutton);
        commandbutton.setBounds(270,250,90,20);
        commandbutton.addActionListener(
                        new ActionListener()
                        {
                            public void actionPerformed( ActionEvent e )
                            {
                                String sqlp = parancssor.getText();
                                //JOptionPane.showMessageDialog(null,"Megnyomtad a parancsvégrehajtót!","Parancsvégrehajtó",1);
                                try
                                {
                                    Kapcsolodas(user, pswd); //Kapcsolódás
                                    s = conn.createStatement();
                                    s.execute(sqlp);
                                    s.close();
                                    Lekapcsolodas();
                                    JOptionPane.showMessageDialog(null, "Sikeres végrehajtás!", "SQL Siker!", 1);
                                    
                                } 
                                catch (Exception ex) 
                                {
                                    JOptionPane.showMessageDialog(null, "Nem sikerült a végrehajtás!"+ex.getMessage(), "Hiba van!", 2);
                                }
                            }
                        }
                
                                       );
                
        // INNEN KEZDÖDIK AZ 1. Tábla - vagyis 1.Fül(Tab) ***********************************************************
        jp1.setLayout(null);
                
        blist = new JButton();
        blist.setText("Lista");
        jp1.add(blist);
        blist.setBounds(10,10,100,20);
        blist.addActionListener( 
                new ActionListener()
                {
                    public void actionPerformed( ActionEvent e ) 
                    {
                        //JOptionPane.showMessageDialog(null,"Tábla 1 Listázógombja",up,1);
                        Object Mezonevekp[] = {"Cegjegyzekszam", "Nev", "Ir_szam", "Varos", "Utca", "Hsz", "Tel", "Web", "Email"};
                        dtmp = new Szallitom(Mezonevekp,0 );
                        Kapcsolodas(user, pswd);
                        if (dtmp.getRowCount()>0) for (int i=0; i < dtmp.getRowCount(); i++)
                        {dtmp.removeRow(i);i--;}
                        SzallitoLista();
                        Lekapcsolodas();
                        Point bs = getLocation(); int bsx = (int)bs.getX(); int bsy = (int)bs.getY();
                        SzallitoKiir ak = new SzallitoKiir(null, dtmp, bsx, bsy); //Példányosítás
                        ak.setVisible(true); //Megjelenítés                          
                    }
                }
                               );

        bbeszur = new JButton();
        bbeszur.setText("Beszúrás");
        jp1.add(bbeszur);
        bbeszur.setBounds(10,40,100,20);
        bbeszur.addActionListener(
                new ActionListener()
                {
                    public void actionPerformed( ActionEvent e ) 
                    {
                        Point bs = getLocation(); int bsx = (int)bs.getX(); int bsy = (int)bs.getY();
                        SzallitoBe us = new SzallitoBe(null, bsx, bsy);us.setVisible(true);
                        if (us.kiLep()==2)  //Visszaadott érték vizsgálat
                        {
                            try
                            {
                                //String sqlp = "insert into O_Szallito(Cegjegyzekszam, Nev, Ir_Szam, Varos, Utca, Hsz, Tel, Web, Email) values(?, ?, ?, ?, ?, ?, ?, ?, ?)";
                                String sqlp ="{call pkg_O_Szallito.Beszur_O_Szallito(?, ?, ?, ?, ?, ?, ?, ?, ?)}";
                                Kapcsolodas(user, pswd); //Kapcsolódás
                                CallableStatement cs = conn.prepareCall(sqlp);
                                //ps = conn.prepareStatement(sqlp); //Kifejezés előkészítés
                                cs.setString(1, us.getCegjegyzekszam());
                                cs.setString(2, us.getNev());
                                cs.setInt(3, us.getIr_Szam());
                                cs.setString(4, us.getVaros());
                                cs.setString(5, us.getUtca());
                                cs.setString(6, us.getHsz());
                                cs.setString(7, us.getTel());
                                cs.setString(8, us.getWeb());
                                cs.setString(9, us.getEmail());
                                cs.execute(); //Parancs végrehajtása
                                cs.close(); //Kifejezés lezárása
                                Lekapcsolodas();
                                JOptionPane.showMessageDialog(null,"Rekord Beszúrva! ",up,1); //Üzenet kiírása
                                
                            }
                            catch (Exception ex)
                            {
                                JOptionPane.showMessageDialog(null,"SQL hiba: "+ex.getMessage(),up,2);
                            }
                        }
                    }
                }
                                 );
                
        bmodosit = new JButton();
        bmodosit.setText("Módosítás");
        jp1.add(bmodosit);
        bmodosit.setBounds(10,70,100,20);
        bmodosit.addActionListener(
                new ActionListener()
                {
                    public void actionPerformed( ActionEvent e )
                    {
                        Object Mezonevekt[] = {"Jel","Cegjegyzekszam", "Nev", "Ir_Szam", "Varos", "Utca", "Hsz", "Tel", "Web", "Email"};
                        ttmp = new Szallitomj( Mezonevekt,0 );
                        Kapcsolodas(user, pswd);
                        if (ttmp.getRowCount()>0) for (int i=0; i < ttmp.getRowCount(); i++)
                        {
                            ttmp.removeRow(i);
                            i--;
                        }
                        SzallitoListaJel();
                        Lekapcsolodas();
                        Point bs = getLocation(); int bsx = (int)bs.getX(); int bsy = (int)bs.getY();
                        SzallitoMod am = new SzallitoMod(null, ttmp, bsx, bsy);
                        am.setVisible(true);
                        if (am.kiLep() == 2)
                        {
                            Kapcsolodas(user, pswd);
                            if (am.getNev().length() > 0) SzallitoUpdate("Nev='"+am.getNev()+
                            "' where Cegjegyzekszam= '"+(String)ttmp.getValueAt(am.getJel(),1)+"'");
                            if (am.getIr_Szam().length() > 0) SzallitoUpdate("Ir_Szam='"+am.getIr_Szam()+
                            "' where Cegjegyzekszam= '"+(String)ttmp.getValueAt(am.getJel(),1)+"'");
                            if (am.getVaros().length() > 0) SzallitoUpdate("Varos='"+am.getVaros()+
                            "' where Cegjegyzekszam= '"+(String)ttmp.getValueAt(am.getJel(),1)+"'");
                            if (am.getUtca().length() > 0) SzallitoUpdate("Utca='"+am.getUtca()+
                            "' where Cegjegyzekszam= '"+(String)ttmp.getValueAt(am.getJel(),1)+"'");
                            if (am.getHsz().length() > 0) SzallitoUpdate("Hsz='"+am.getHsz()+
                            "' where Cegjegyzekszam= '"+(String)ttmp.getValueAt(am.getJel(),1)+"'");
                            if (am.getTel().length() > 0) SzallitoUpdate("Tel='"+am.getTel()+
                            "' where Cegjegyzekszam= '"+(String)ttmp.getValueAt(am.getJel(),1)+"'");
                            if (am.getWeb().length() > 0) SzallitoUpdate("Web='"+am.getWeb()+
                            "' where Cegjegyzekszam= '"+(String)ttmp.getValueAt(am.getJel(),1)+"'");
                            if (am.getEmail().length() > 0) SzallitoUpdate("Email='"+am.getEmail()+
                            "' where Cegjegyzekszam= '"+(String)ttmp.getValueAt(am.getJel(),1)+"'");                            
                        }
                    }
                }
                                  );
        btorol = new JButton();
        btorol.setText("Törlés");
        jp1.add(btorol);
        btorol.setBounds(10,100,100,20);
        btorol.addActionListener(
                new ActionListener()
                {
                    public void actionPerformed( ActionEvent e ) 
                    {
                        Kapcsolodas(user, pswd);
                        if (ttmp.getRowCount()>0) for (int i=0; i < ttmp.getRowCount(); i++)
                        {
                            ttmp.removeRow(i);
                            i--;
                        }
                        SzallitoListaJel();
                        Lekapcsolodas();
                        Point bs = getLocation(); int bsx = (int)bs.getX(); int bsy = (int)bs.getY();
                        SzallitoTorol at = new SzallitoTorol(null, ttmp, bsx, bsy);
                        at.setVisible(true);
                        if (at.kiLep() == 2) 
                        {
                            int db=0;
                            Kapcsolodas(user, pswd);
                            for(int x = 0; x < ttmp.getRowCount(); x++)
                                if (at.getJel(x)) 
                                {
                                    SzallitoTorol((String)ttmp.getValueAt(x,1));    //Törli a rekordot az azonosító alapján
                                    db++;
                                }
                            Lekapcsolodas();
                            JOptionPane.showMessageDialog(null,db+" rekord törölve!",up,1);
                        }
                    }
                }
                                );
        
        // INNEN KEZDÖDIK A 2. PANEL ***********************************************************
        jp2.setLayout(null);
        blist = new JButton();
        blist.setText("Lista");
        jp2.add(blist);
        blist.setBounds(10,10,100,20);
        blist.addActionListener( 
                new ActionListener()
                {
                    public void actionPerformed( ActionEvent e ) 
                    {
                        Object Mezonevekp[] = {"Vonalkod", "Nev", "Gyarto", "Ar", "Alkohol", "Kiszereles", "Szarmazasi_hely", "Szallito"};
                        dtmp = new Szallitom(Mezonevekp,0 ); //Táblafej létrehozása, mezőnevekkel
                        Kapcsolodas(user, pswd); //Kapcsolódás, létező táblatartalom törlése
                        if (dtmp.getRowCount()>0) for (int i=0; i < dtmp.getRowCount(); i++)
                        {dtmp.removeRow(i);i--;}
                        ItalLista(); //Táblatartalom betöltése
                        Lekapcsolodas();
                        Point bs = getLocation(); int bsx = (int)bs.getX(); int bsy = (int)bs.getY();
                        //Helymeghatározás
                        ItalKiir ak = new ItalKiir(null, dtmp, bsx, bsy); //Példányosítás
                        ak.setVisible(true); //Megjelenítés    
                    }
                }
                               );


        bbeszur = new JButton();
        bbeszur.setText("Beszúrás");
        jp2.add(bbeszur);
        bbeszur.setBounds(10,40,100,20);
        bbeszur.addActionListener(
                new ActionListener()
                {
                    public void actionPerformed( ActionEvent e ) 
                    {
                        Point bs = getLocation(); int bsx = (int)bs.getX(); int bsy = (int)bs.getY();
                        ItalBe us = new ItalBe(null, bsx, bsy);us.setVisible(true);
                        if (us.kiLep()==2) //Visszaadott érték vizsgálat
                        {
                            try
                            {
                                //String sqlp = "insert into O_Ital(Vonalkod, Nev, Gyarto, Ar, Alkohol, Kiszereles, Szarmazasi_Hely, Szallito) values(?, ?, ?, ?, ?, ?, ?, ?)";
                                String sqlp ="{call pkg_O_Ital.Beszur_O_Ital(?, ?, ?, ?, ?, ?, ?, ?)}";
                                Kapcsolodas(user, pswd); //Kapcsolódás
                                CallableStatement cs = conn.prepareCall(sqlp);
                                //ps = conn.prepareStatement(sqlp); //Kifejezés előkészítés
                                cs.setString(1, us.getVonalkod());
                                cs.setString(2, us.getNev());
                                cs.setString(3, us.getGyarto());
                                cs.setInt(4, us.getAr());
                                cs.setInt(5, us.getAlkohol());
                                cs.setInt(6, us.getKiszereles());
                                cs.setString(7, us.getSzarmhely());
                                cs.setString(8, us.getSzallito());
                                cs.execute(); //Parancs végrehajtása
                                cs.close(); //Kifejezés lezárása
                                Lekapcsolodas();
                                JOptionPane.showMessageDialog(null,"Rekord Beszúrva! ",up,1); //Üzenet kiírása
                            }
                            catch (Exception ex)
                            {
                                JOptionPane.showMessageDialog(null,"SQL hiba: "+ex.getMessage(),up,2);
                            }
                        }
                    }
                }
                                 );
        
       
        bmodosit = new JButton();
        bmodosit.setText("Módosítás");
        jp2.add(bmodosit);
        bmodosit.setBounds(10,70,100,20);
        bmodosit.addActionListener(
                new ActionListener()
                {
                    public void actionPerformed( ActionEvent e )
                    {
                        Object Mezonevekt2[] = {"Jel","Vonalkod", "Nev", "Gyarto", "Ar", "Alkohol", "Kiszereles", "Szarmazasi_hely", "Szallito"};
                        ttmp = new Szallitomj( Mezonevekt2,0 );                         
                        Kapcsolodas(user, pswd);
                        if (ttmp.getRowCount()>0) for (int i=0; i < ttmp.getRowCount(); i++)
                        {
                            ttmp.removeRow(i);
                            i--;
                        }
                        ItalListaJel();
                        Lekapcsolodas();
                        Point bs = getLocation(); int bsx = (int)bs.getX(); int bsy = (int)bs.getY();
                        ItalMod am = new ItalMod(null, ttmp, bsx, bsy);
                        am.setVisible(true);
                        if (am.kiLep() == 2)
                        {
                            Kapcsolodas(user, pswd);
                            if (am.getNev().length() > 0) ItalUpdate("Nev='"+am.getNev()+
                             "' where Vonalkod= '"+(String)ttmp.getValueAt(am.getJel(),1)+"'");
                            if (am.getGyarto().length() > 0) ItalUpdate("Gyarto='"+am.getGyarto()+
                             "' where Vonalkod= '"+(String)ttmp.getValueAt(am.getJel(),1)+"'");
                            if (am.getAr().length() > 0) ItalUpdate("Ar='"+am.getAr()+
                             "' where Vonalkod= '"+(String)ttmp.getValueAt(am.getJel(),1)+"'");
                            if (am.getAlkohol().length() > 0) ItalUpdate("Alkohol='"+am.getAlkohol()+
                             "' where Vonalkod= '"+(String)ttmp.getValueAt(am.getJel(),1)+"'");
                            if (am.getKiszereles().length() > 0) ItalUpdate("Kiszereles='"+am.getKiszereles()+
                             "' where Vonalkod= '"+(String)ttmp.getValueAt(am.getJel(),1)+"'");
                            if (am.getSzarmhely().length() > 0) ItalUpdate("Szarmazasi_Hely='"+am.getSzarmhely()+
                             "' where Vonalkod= '"+(String)ttmp.getValueAt(am.getJel(),1)+"'"); 
                            if (am.getSzallito().length() > 0) ItalUpdate("Szallito='"+am.getSzallito()+
                             "' where Vonalkod= '"+(String)ttmp.getValueAt(am.getJel(),1)+"'");                            
 
                        }
                    }
                }
                                  );
        btorol = new JButton();
        btorol.setText("Törlés");
        jp2.add(btorol);
        btorol.setBounds(10,100,100,20);
        btorol.addActionListener(
                new ActionListener()
                {
                    public void actionPerformed( ActionEvent e ) 
                    {
                        Kapcsolodas(user, pswd);
                        if (ttmp.getRowCount()>0) for (int i=0; i < ttmp.getRowCount(); i++)
                        {
                            ttmp.removeRow(i);
                            i--;
                        }
                        ItalListaJel();
                        Lekapcsolodas();
                        Point bs = getLocation(); int bsx = (int)bs.getX(); int bsy = (int)bs.getY();
                        ItalTorol at = new ItalTorol(null, ttmp, bsx, bsy);
                        at.setVisible(true);
                        if (at.kiLep() == 2)
                        {
                            int db=0;
                            Kapcsolodas(user, pswd);
                            for(int x = 0; x < ttmp.getRowCount(); x++)
                                if (at.getJel(x))
                                {
                                    ItalTorol((String)ttmp.getValueAt(x,1));    //Törli a rekordot az azonosító alapján
                                    db++;
                                }
                            Lekapcsolodas();
                            JOptionPane.showMessageDialog(null,db+" rekord törölve!",up,1);
                            
                        }
                    }
                }
                                );
        // INNEN KEZDÖDIK A 3. PANEL ***********************************************************
        jp3.setLayout(null);
               
        blist = new JButton();
        blist.setText("Lista");
        jp3.add(blist);
        blist.setBounds(10,10,100,20);
        blist.addActionListener( 
                new ActionListener()
                {
                    public void actionPerformed( ActionEvent e ) 
                    {
                        //JOptionPane.showMessageDialog(null," 3. TÁBLA LISTAGOMB",up,1);
                        Object Mezonevekp[] = {"Szemigszam", "Nev", "Ir_szam", "Varos", "Utca", "Hsz", "Foglalkozas"};
                        dtmp = new Szallitom(Mezonevekp,0 );
                        Kapcsolodas(user, pswd);
                        if (dtmp.getRowCount()>0) for (int i=0; i < dtmp.getRowCount(); i++)
                        {dtmp.removeRow(i);i--;}
                        VevoLista();
                        Lekapcsolodas();
                        Point bs = getLocation(); int bsx = (int)bs.getX(); int bsy = (int)bs.getY();
                        VevoKiir ak = new VevoKiir(null, dtmp, bsx, bsy); //Példányosítás
                        ak.setVisible(true); //Megjelenítés                                     
                    }
                }
                               );
        
        bbeszur = new JButton();
        bbeszur.setText("Beszúrás");
        jp3.add(bbeszur);
        bbeszur.setBounds(10,40,100,20);
        bbeszur.addActionListener(
                new ActionListener()
                {
                    public void actionPerformed( ActionEvent e ) 
                    {
                        //JOptionPane.showMessageDialog(null," 3. TÁBLA BESZURGOMB",up,1);
                        Point bs = getLocation(); int bsx = (int)bs.getX(); int bsy = (int)bs.getY();
                        VevoBe us = new VevoBe(null, bsx, bsy);us.setVisible(true);
                        if (us.kiLep()==2) //Visszaadott érték vizsgálat
                        {
                            try
                            {
                                String sqlp ="{call pkg_O_Vevo.Beszur_O_Vevo(?, ?, ?, ?, ?, ?, ?)}";
                                Kapcsolodas(user, pswd); //Kapcsolódás
                                CallableStatement cs = conn.prepareCall(sqlp);
                                cs.setString(1, us.getSzemigszam());
                                cs.setString(2, us.getNev());
                                cs.setInt(3, us.getIr_Szam());
                                cs.setString(4, us.getVaros());
                                cs.setString(5, us.getUtca());
                                cs.setString(6, us.getHsz());
                                cs.setString(7, us.getFoglalkozas());
                                cs.execute();
                                cs.close();
                                Lekapcsolodas();
                                JOptionPane.showMessageDialog(null,"Rekord Beszúrva! ",up,1); //Üzenet kiírása
                            } 
                            catch (Exception ex)
                            {
                                JOptionPane.showMessageDialog(null,"SQL hiba: "+ex.getMessage(),up,2);
                            }
                        }

                    }
                }
                                 );   
        
        bmodosit = new JButton();
        bmodosit.setText("Módosítás");
        jp3.add(bmodosit);
        bmodosit.setBounds(10,70,100,20);
        bmodosit.addActionListener(
                new ActionListener()
                {
                    public void actionPerformed( ActionEvent e )
                    {
                        //JOptionPane.showMessageDialog(null," 3. TÁBLA MODOSITGOMB",up,1);
                        Object Mezonevekt2[] = {"Jel","Szemigszam", "Nev", "Ir_Szam", "Varos", "Utca", "Hsz", "Foglalkozas"};
                        ttmp = new Szallitomj( Mezonevekt2,0 );                         
                        Kapcsolodas(user, pswd);
                        if (ttmp.getRowCount()>0) for (int i=0; i < ttmp.getRowCount(); i++)
                        {
                            ttmp.removeRow(i);
                            i--;
                        }
                        VevoListaJel();
                        Lekapcsolodas();
                        Point bs = getLocation(); int bsx = (int)bs.getX(); int bsy = (int)bs.getY();
                        VevoMod am = new VevoMod(null, ttmp, bsx, bsy);
                        am.setVisible(true);
                        if (am.kiLep() == 2)
                        {
                            Kapcsolodas(user, pswd);
                            if (am.getNev().length() > 0) VevoUpdate("Nev='"+am.getNev()+
                             "' where Szemigszam= '"+(String)ttmp.getValueAt(am.getJel(),1)+"'");
                            if (am.getIr_Szam().length() > 0) VevoUpdate("Ir_Szam='"+am.getIr_Szam()+
                             "' where Szemigszam= '"+(String)ttmp.getValueAt(am.getJel(),1)+"'");
                            if (am.getVaros().length() > 0) VevoUpdate("Varos='"+am.getVaros()+
                             "' where Szemigszam= '"+(String)ttmp.getValueAt(am.getJel(),1)+"'");
                            if (am.getUtca().length() > 0) VevoUpdate("Utca='"+am.getUtca()+
                             "' where Szemigszam= '"+(String)ttmp.getValueAt(am.getJel(),1)+"'");
                            if (am.getHsz().length() > 0) VevoUpdate("Hsz='"+am.getHsz()+
                             "' where Szemigszam= '"+(String)ttmp.getValueAt(am.getJel(),1)+"'");
                            if (am.getFoglalkozas().length() > 0) VevoUpdate("Foglalkozas='"+am.getFoglalkozas()+
                             "' where Szemigszam= '"+(String)ttmp.getValueAt(am.getJel(),1)+"'");                            
                        }
 
                    }
                }
                                  );
        
        btorol = new JButton();
        btorol.setText("Törlés");
        jp3.add(btorol);
        btorol.setBounds(10,100,100,20);
        btorol.addActionListener(
                new ActionListener()
                {
                    public void actionPerformed( ActionEvent e ) 
                    {
                        //JOptionPane.showMessageDialog(null," 3. TÁBLA TORLOGOMB",up,1);
                        Kapcsolodas(user, pswd);
                        if (ttmp.getRowCount()>0) for (int i=0; i < ttmp.getRowCount(); i++)
                        {
                            ttmp.removeRow(i);
                            i--;
                        }
                        VevoListaJel();
                        Lekapcsolodas();
                        Point bs = getLocation(); int bsx = (int)bs.getX(); int bsy = (int)bs.getY();
                        VevoTorol at = new VevoTorol(null, ttmp, bsx, bsy);
                        at.setVisible(true);
                        if (at.kiLep() == 2)
                        {
                            int db=0;
                            Kapcsolodas(user, pswd);
                            for(int x = 0; x < ttmp.getRowCount(); x++)
                                if (at.getJel(x))
                                {
                                    VevoTorol((String)ttmp.getValueAt(x,1));    //Törli a rekordot az azonosító alapján
                                    db++;
                                }
                            Lekapcsolodas();
                            JOptionPane.showMessageDialog(null,db+" rekord törölve!",up,1);
                        }
                        
                    }
                }
                                );
        
        // 4. panel - fül - Az O_Naplo Tábla
        jp4.setLayout(null);
        
        blist = new JButton();
        blist.setText("Lista");
        jp4.add(blist);
        blist.setBounds(10,10,100,20);
        blist.addActionListener(
                new ActionListener()
                {
                    public void actionPerformed( ActionEvent e )
                    {
                        //JOptionPane.showMessageDialog(null," Naplo kilistázása!",up,1);
                        Object Mezonevekp[] = {"Muvelet", "Usernev", "Idopont", "Esemeny", "Status"};
                        dtmp = new Szallitom(Mezonevekp,0 ); //Táblafej létrehozása, mezőnevekkel
                        Kapcsolodas(user, pswd); //Kapcsolódás, létező táblatartalom törlése
                        if (dtmp.getRowCount()>0) for (int i=0; i < dtmp.getRowCount(); i++)
                        {dtmp.removeRow(i);i--;}
                        NaploLista(); //Táblatartalom betöltése
                        
                        Point bs = getLocation(); int bsx = (int)bs.getX(); int bsy = (int)bs.getY();
                        NaploKiir ak = new NaploKiir(null, dtmp, bsx, bsy); //Példányosítás
                        ak.setVisible(true); //Megjelenítés
                        
                        String sqlp= "UPDATE O_Naplo SET STATUS=1 WHERE STATUS=0";
                    
                        try
                        {
                            s = conn.createStatement();
                            s.executeUpdate(sqlp);
                            s.close();
                        } 
                        catch (Exception ex)
                        {
                            JOptionPane.showMessageDialog(null, "Nem sikerült Olvasottra állitani a Naplót!"+ex.getMessage(), up, 2);
                        }
                        
                        sqlp ="{call pkg_O_Naplo.NaploTorol()}";
                        try 
                        {
                            CallableStatement cs = conn.prepareCall(sqlp);
                            cs.execute();
                            cs.close();
                        }
                        catch (Exception ex)
                        {
                            JOptionPane.showMessageDialog(null, "Nem sikerült törölni a már olvasott Naplóbejegyzéseket!"+ex.getMessage(), up, 2);
                        }
                        
                        Lekapcsolodas();
                        
                        
                    }
                }
                
                               );
        
    }


    public static void main(String[] args) 
    {
        Ablak ab = new Ablak();
        ab.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        ab.setVisible(true);
    }
}
